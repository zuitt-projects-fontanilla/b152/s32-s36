
const jwt = require("jsonwebtoken");
const secret = "CourseBookingApi";

module.exports.createAccesToken = (user) => {

	//console.log(user);

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//console.log(data);


	return jwt.sign(data,secret,{});

}

module.exports.verify = (req,res,next) => {

	//req.headers.authorization contains sensitive data especially our token

	//console.log(req.headers.authorization);
	let token = req.headers.authorization;


	//if we are not passing token in our request authorization or in our postman, then here
	//in our api req.header.authorization will be undefined.

	//this if statement will first check if toke var contains undefined. or a proper jwt. if it is 
	//undefined we wil check its token data type with typeof then send a message
	// to the client

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No token."})
	}else {

		//console.log(token) token before slice
		token = token.slice(7,token.length);
		//console.log(token);

		jwt.verify(token,secret,function(err,decodedToken){

			if(err){
				return res.send({
					auth:"Failed",
					message: err.message
				});
			}else {
				//console.log(decodedToken);

				req.user = decodedToken;
				
				//next() will let us proceed to the next middleware or controller
				next();
			
			}

		})

		
		
	}

}



module.exports.verifyAdmin = (req,res,next) => {

	//console.log(req.user);

	if(req.user.isAdmin){

		next();
	}else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}

}







const Course = require("../models/Course");



module.exports.addCourse = (req,res) => {

	console.log(req,res)

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		
	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error))

}

module.exports.getAllCourses = (req,res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getSingleCourse = (req, res) => {

	

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));

}


module.exports.updateCourse = (req,res) => {

	//console.log(req.params.id);//

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updateCourse => res.send(updateCourse))
	.catch(err => res.send(err));


}

module.exports.archiveCourse = (req,res) => {

	let updates = {

		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(results => res.send(results))
	.catch(err => res.send(err));
}

module.exports.activateCourse = (req,res) => {

	let updates = {

		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(results => res.send(results))
	.catch(err => res.send(err));



}

module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(results => res.send(results))
	.catch(err => res.send(err));
}

module.exports.getArchiveCourses = (req,res) => {

	Course.find({isActive:false})
	.then(results => res.send(results))
	.catch(err => res.send(err));
}

module.exports.findCourses = (req,res) => {

	//console.log(req.body)

	Course.find({name:req.body.name})
	.then(results =>{
		if(results.length === 0){
			return res.send("No Course Found.")
		} else {
			return res.send(results)
		}
	})
	.catch(err => res.send(err));
}

module.exports.findPrices = (req,res) => {

	//console.log(req.body)

	Course.find({price:req.body.price})
	.then(results =>{
		if(results.length === 0){
			return res.send("No Course Found.")
		}else {
			return res.send(results)
		}
	})
	.catch(err => res.send(err));
}

module.exports.getEnrollees = (req,res) => {

	Course.findById(req.params.id)
	.then(result => res.send(result.enrollees))
	.catch(err => res.send(err));
}










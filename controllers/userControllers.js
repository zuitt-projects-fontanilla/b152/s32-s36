//import user model
const User = require("../models/User");

const Course = require("../models/Course");

const bcrypt = require("bcrypt");

const auth = require("../auth");
//console.log(auth);

module.exports.registerUser = (req,res) => {

	console.log(req.body);

/*
	bcrypt adds a layer of security to our user's password.
	What bcrypt does is hash our password into a randomized character version 
	of the orginial String.

	bcrypt.hashSync(<stringToBeHashed>,<saltRounds>)

	Salt-Rounds are the number of times the characters in the hash are 
	randomized.
*/

//create a var to store the hashed/encrypted password. Then this new hashed pw will be 
// we save in our database.

const hashedPW = bcrypt.hashSync(req.body.password,10);
//console.log(hashedPW);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));

}

module.exports.getAllUsers = (req,res) => {

	User.find({})
	.then(result => res.send(user))
	.catch(error => res.send(user))

}

module.exports.loginUser = (req,res) => {

	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {

		//console.log(foundUser);
		if(foundUser === null){
			return res.send("No User Found.")
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			console.log(isPasswordCorrect);
		
			//syntax: bcrypt.compareSync(<string>,<hashedString>)
		
			if(isPasswordCorrect){
				
				return res.send({accessToken: auth.createAccesToken(foundUser)})
			}else {
				return res.send("Incorrect Password.")
			}

		}
	})
	.catch(err => res.send(err));

}

module.exports.getUserDetails = (req,res) => {

	console.log(req.user);
	
//Model.findById() will allow us to look for a document using id
//it is equivalent to = db.collection.findOne({_id: "id"})
	
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));



	//res.send("testing for verify");
}

module.exports.checkEmailExists = (req,res) => {

	User.findOne({email:req.body.email})
	.then(result => {

		if(result === null){
			return res.send("Email is Available.");
		} else {
			return res.send("Email is already registered!")
		}

	})

	.catch(error => res.send(error));

	
}

module.exports.updateUserDetails = (req,res) => {

	console.log(req.body);
	console.log(req.user.id);

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	
	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
}


module.exports.updateAdmin = (req,res) => {

	//console.log(req.user);

	console.log(req.params.id)

	let updates = {

		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(results => res.send(results))
	.catch(err => res.send(err));

}

module.exports.enroll = async (req,res) => {
	//console.log("test enroll route")

	console.log(req.user.id)
	console.log(req.body.courseId)
	console.log(req.user)

	if(req.user.isAdmin){
		return res.send("Action Forbidden.")
	}
	//if not we continue

	//find the user:


	/* 

	async allows us to make our function asynchronous.
	which means, that instead of JS regular behaviour of running each
	code line by line, it will alow us to wait for the results of our function.
	await keyword allows us to wait for the function to finish before procedding
*/


	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//console.log(user);
	
		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment);

		//save the changes made to our user document and return the value of saving
		//our document.
		//If we properly saved our document isUserUpdated will contain the boolean true.
		//If we catch an error, isUserUpdated wil contain the error message.

		return user.save().then(user => true).catch(err => err.message)

	})

	//console.log(isUserUpdated);
	//if isUserUpdated doesn't contain the boolean true, we will stop our process and returen
	//a res.senc() to our client with our message.

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {


		//console.log(course);

		let enrollee = {
			userId: req.user.id
		}
		
		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message)

	})

	//console.log(isCourseUpdated);


	if(isCourseUpdated !== true) {
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}

}

module.exports.getEnrollments = (req,res) => {

	

	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err));
}


















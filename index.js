const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000


mongoose.connect("mongodb+srv://adrian:adrian@cluster0.1nbs0.mongodb.net/bookingAPI152?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection;
db.on("error",console.error.bind(console, "Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

app.use(express.json());

const userRoutes = require('./routes/userRoutes');

app.use('/users',userRoutes);


const courseRoutes = require('./routes/courseRoutes');

app.use('/courses',courseRoutes);


app.listen(port,()=>console.log(`Server running at localhost:4000`));



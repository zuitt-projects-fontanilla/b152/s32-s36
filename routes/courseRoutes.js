const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


//we have to authorize only some users who have the proper authority
router.post("/",verify,verifyAdmin,courseControllers.addCourse);

router.get("/",courseControllers.getAllCourses);

router.get('/getSingleCourse/:id',courseControllers.getSingleCourse);


router.put('/:id',verify,verifyAdmin,courseControllers.updateCourse);

router.put('/archive/:id',verify,verifyAdmin,courseControllers.archiveCourse);

router.put('/activate/:id',verify,verifyAdmin,courseControllers.activateCourse);

router.get('/getActiveCourses',courseControllers.getActiveCourses);

router.get('/getArchiveCourses',verify,verifyAdmin,courseControllers.getArchiveCourses);

router.post('/findCourses',courseControllers.findCourses);

router.post('/findPrices',courseControllers.findPrices);

router.get('/getEnrollees/:id',verify,verifyAdmin,courseControllers.getEnrollees);


module.exports = router;


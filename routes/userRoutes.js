const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers")

const auth = require("../auth");

const {verify,verifyAdmin} = auth;

//console.log(verify);

//User registration
router.post("/",userControllers.registerUser);

router.get("/",userControllers.getAllUsers);

router.post('/login',userControllers.loginUser);

//verify method from auth.js will be ues as a middleware. Middleware are functions we can use
//around our app. In the context of expressjs route, they are functions we 
//add in the rout and can recieve the request, response and next objects.
//We can have more than 1 middleware before the controller.
//router.get('<endpoint>'.middleware, controller);

//When a route has middlewares and controllers, we will run through the middleware first
//before we get to the controller.

//Note: Routes that have verify as a middleware would require us to pass a token from postman
router.get('/getUserDetails',verify,userControllers.getUserDetails);

router.post('/checkEmailExists/',userControllers.checkEmailExists);

router.put('/updateUserDetails/',verify,userControllers.updateUserDetails);

router.put('/updateAdmin/:id',verify,verifyAdmin,userControllers.updateAdmin);

router.post('/enroll',verify,userControllers.enroll);

router.get('/getEnrollments',verify,userControllers.getEnrollments);

module.exports = router;

